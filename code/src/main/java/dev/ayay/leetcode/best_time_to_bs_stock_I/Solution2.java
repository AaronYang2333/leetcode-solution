package dev.ayay.leetcode.best_time_to_bs_stock_I;

/**
 * 1. 动态规划做题步骤
 *
 * 明确 dp(i) 应该表示什么（二维情况：dp(i)(j)）；
 * 根据 dp(i) 和 dp(i−1) 的关系得出状态转移方程；
 * 确定初始条件，如 dp(0)。
 *
 * 作者：腐烂的橘子
 * 链接：https://leetcode.cn/problems/best-time-to-buy-and-sell-stock/solutions/139559/gu-piao-wen-ti-python3-c-by-z1m/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Solution2 {
  public int maxProfit(int[] prices) {
    int[] dp = new int[prices.length];
    int minCost = prices[0];
    dp[0] = 0;
    for (int i = 1; i < prices.length; i++) {
      minCost = Math.min(minCost, prices[i]);
      // dp[i] 表示第i天获得的最大利润
      dp[i] = Math.max(dp[i - 1], prices[i] - minCost);
    }
    return dp[prices.length - 1];
  }

  public static void main(String[] args) {
    System.out.println(new Solution2().maxProfit(new int[] {7, 1, 5, 3, 6, 4}));
  }
}
