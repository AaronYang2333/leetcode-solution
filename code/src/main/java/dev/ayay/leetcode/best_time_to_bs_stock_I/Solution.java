package dev.ayay.leetcode.best_time_to_bs_stock_I;

/**
 * 1. 动态规划做题步骤
 *
 * 明确 dp(i) 应该表示什么（二维情况：dp(i)(j)）；
 * 根据 dp(i) 和 dp(i−1) 的关系得出状态转移方程；
 * 确定初始条件，如 dp(0)。
 *
 * 作者：腐烂的橘子
 * 链接：https://leetcode.cn/problems/best-time-to-buy-and-sell-stock/solutions/139559/gu-piao-wen-ti-python3-c-by-z1m/
 * 来源：力扣（LeetCode）
 * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
 */
public class Solution {
  public int maxProfit(int[] prices) {
    int cost = prices[0];
    int maxProfit = 0;
    for (int price : prices) {
      cost = Math.min(cost, price);
      maxProfit = Math.max(maxProfit, price - cost);
    }
    return maxProfit;
  }

  public static void main(String[] args) {
    System.out.println(new Solution().maxProfit(new int[] {7, 1, 5, 3, 6, 4}));
  }
}
