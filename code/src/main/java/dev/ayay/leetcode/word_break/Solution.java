package dev.ayay.leetcode.word_break;

import java.util.List;

public class Solution {
  public boolean wordBreak(String s, List<String> wordDict) {
    boolean[] dp = new boolean[s.length() + 1];
    dp[0] = true;

    for (int i = 0; i < s.length(); i++) {
      if (!dp[i]) {
        continue;
      }
      for (String word : wordDict) {
        if (i + word.length() <= s.length() && s.substring(i).startsWith(word)) {
          dp[i + word.length()] = true;
        }
      }
    }

    return dp[s.length()];
  }

  public static void main(String[] args) {
    boolean b = new Solution().wordBreak("catsandog", List.of("cats", "dog", "sand", "and", "cat"));
    System.out.println(b);
  }
}
