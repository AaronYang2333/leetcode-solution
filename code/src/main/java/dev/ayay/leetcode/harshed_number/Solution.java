package dev.ayay.leetcode.harshed_number;

public class Solution {
  public int sumOfTheDigitsOfHarshadNumber(int x) {
    if (x < 10) {
      return x;
    }
    int sum = 0;
    int original = x;

    while (x >= 10) {
      sum += x % 10;
      x = x / 10;
    }
    sum += x;
    return original % sum == 0 ? sum : -1;
  }

  public static void main(String[] args) {
    for (int i = 0; i < 101; i++) {
      System.out.println(i + " -> " + new Solution().sumOfTheDigitsOfHarshadNumber(i));
    }
  }
}
