package dev.ayay.leetcode.jump_game;

/**
 * https://leetcode.cn/problems/jump-game/submissions/542468643/?envType=study-plan-v2&envId=top-interview-150
 */
public class Solution {
  //  public boolean canJump(int[] nums) {
  //    int[] dp = new int[nums.length];
  //    dp[0] = nums[0];
  //    for (int i = 1; i < nums.length - 1; i++) {
  //      dp[i] = Math.max(dp[i - 1], nums[i] + i);
  //      if (dp[i] >= nums.length - 1) {
  //        return true;
  //      }
  //      if (dp[i] == i) {
  //        return false;
  //      }
  //    }
  //
  //    if (dp[nums.length - 1] >= nums.length - 1) {
  //      return true;
  //    } else {
  //      return false;
  //    }
  //  }

  public boolean canJump(int[] nums) {
    int[] dp = new int[nums.length];
    dp[0] = nums[0];
    if (dp[0] >= nums.length - 1) {
      return true;
    }
    for (int i = 1; i < nums.length; i++) {
      if (dp[i - 1] > 0 && dp[i - 1] + nums[i] > i) {
        dp[i] = Math.max(dp[i - 1], i + nums[i]);
      } else {
        return false;
      }
      if (dp[i] >= nums.length - 1) {
        return true;
      }
    }

    if (dp[nums.length - 1] >= nums.length - 1) {
      return true;
    } else {
      return false;
    }
  }

  public static void main(String[] args) {
    System.out.println(new Solution().canJump(new int[] {1, 0}));
  }
}
