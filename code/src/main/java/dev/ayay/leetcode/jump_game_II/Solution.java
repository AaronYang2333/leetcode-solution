package dev.ayay.leetcode.jump_game_II;

public class Solution {

  //    public int jump(int[] nums) {
  //        int temp_max = 100000;
  //        int[][] dp = new int[nums.length][nums.length];
  //        for (int i = 0; i < nums.length; i++) {
  //            for (int j = 0; j < nums.length; j++) {
  //                dp[i][j] = temp_max;
  //            }
  //        }
  //        dp[0][0] = 0;
  //        for (int i = 1; i <= nums[0] && i < nums.length; i++) {
  //            dp[0][i] = 1;
  //        }
  //
  //        for (int i = 1; i < nums.length; i++) {
  //            int temp_step = i + 1;
  //            for (int j = 0; j <= Math.max(nums[i - 1], nums[i]); j++) {
  //                if (i + j <= nums.length - 1) {
  //                    dp[i][i + j] = temp_step;
  //                }
  //            }
  //        }
  //
  //        int min_step = temp_max;
  //        for (int i = 0; i < nums.length; i++) {
  //            min_step = Math.min(min_step, dp[i][nums.length - 1]);
  //        }
  //
  //        return min_step;
  //    }

  public int jump(int[] nums) {
    int result = 0;
    int start = 0;
    int end = 1;
    while (end < nums.length) {
      int range = 0;
      for (int i = start; i < end; i++) {
        range = Math.max(range, i + nums[i]);
      }

      start = end;
      end = range + 1;
      result++;
    }

    return result;
  }

  public static void main(String[] args) {
    System.out.println(new Solution().jump(new int[] {1, 2, 1, 1, 1}));
  }
}
