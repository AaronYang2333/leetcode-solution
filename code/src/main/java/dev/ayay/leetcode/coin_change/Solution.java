package dev.ayay.leetcode.coin_change;

public class Solution {

  public int coinChange(int[] coins, int amount) {
    int[] dp = new int[amount + 1];
    int maxValue = amount + 1;
    for (int i = 1; i <= amount; i++) {
      dp[i] = maxValue;
    }
    dp[0] = 0;
    for (int i = 1; i <= amount; i++) {
      for (int coin : coins) {
        if (coin <= i) {
          dp[i] = Math.min(dp[i], dp[i - coin] + 1);
        }
      }
    }

    return dp[amount] == amount + 1 ? -1 : dp[amount];
  }

  public static void main(String[] args) {
    new Solution().coinChange(new int[] {1, 2, 5}, 11);
  }
}
