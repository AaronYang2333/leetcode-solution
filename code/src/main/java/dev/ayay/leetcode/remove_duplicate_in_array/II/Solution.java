package dev.ayay.leetcode.remove_duplicate_in_array.II;

public class Solution {
  public int removeDuplicates(int[] nums) {
    int left = 0;
    int temp_max = Integer.MIN_VALUE;
    for (int i = 1; i < nums.length; i++) {
      if (nums[left] == nums[i]) {
        if (temp_max == Integer.MIN_VALUE) {
          temp_max = nums[i];
          nums[++left] = nums[i];
        }
      } else {
        nums[++left] = nums[i];
        temp_max = Integer.MIN_VALUE;
      }
    }
    return left + 1;
  }

  public static void main(String[] args) {
    int[] nums = new int[] {0, 0, 1, 1, 1, 1, 2, 3, 3};
    Solution solution = new Solution();
    System.out.println(solution.removeDuplicates(nums));
  }
}
