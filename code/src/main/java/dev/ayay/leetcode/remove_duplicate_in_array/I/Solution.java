package dev.ayay.leetcode.remove_duplicate_in_array.I;

/**
 * https://leetcode.cn/problems/remove-duplicates-from-sorted-array/description/?envType=study-plan-v2&envId=top-interview-150
 * 给你一个 非严格递增排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。元素的 相对顺序 应该保持 一致 。然后返回 nums 中唯一元素的个数。
 */
public class Solution {
  public int removeDuplicates(int[] nums) {
    int left = 0;
    for (int i = 0; i < nums.length; i++) {
      if (nums[left] != nums[i]) {
        nums[++left] = nums[i];
      }
    }
    return left + 1;
  }

  public static void main(String[] args) {
    int[] nums = new int[] {1, 1, 2};
    System.out.println(new Solution().removeDuplicates(nums));
  }
}
