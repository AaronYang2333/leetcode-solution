package dev.ayay.leetcode.remove_element_in_array;

import java.util.Arrays;

/**
 * https://leetcode.cn/problems/remove-element/submissions/540891599/?envType=study-plan-v2&envId=top-interview-150
 * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素。元素的顺序可能发生改变。然后返回 nums 中与 val 不同的元素的数量。
 */
public class Solution {
  public int removeElement(int[] nums, int val) {
    int deleted = 0;
    for (int i = 0; i < nums.length; ) {
      if (nums[i] == val) {
        System.arraycopy(nums, i + 1, nums, i, nums.length - i - 1);
        nums[nums.length - 1] = -1;
        deleted += 1;
        i = 0;
      } else {
        i += 1;
      }
    }
    return nums.length - deleted;
  }

  public static void main(String[] args) {
    int[] nums = new int[] {3, 3};
    int i = new Solution().removeElement(nums, 3);
    System.out.println(Arrays.toString(nums));
    System.out.println(i);
  }
}
/**
 * 很直观，如果相等，把后面的整体往前移一位，最后补-1
 *
 * 然后重新遍历，谨防 因为移动而造成的 越过了一部分数据
 */
