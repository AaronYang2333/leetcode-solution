package dev.ayay.leetcode.remove_element_in_array;

import java.util.Arrays;

/**
 * https://leetcode.cn/problems/remove-element/submissions/540891599/?envType=study-plan-v2&envId=top-interview-150
 * 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素。元素的顺序可能发生改变。然后返回 nums 中与 val 不同的元素的数量。
 */
public class Solution2 {
  public int removeElement(int[] nums, int val) {
    int left = 0;
    for (int i = 0; i < nums.length; i++) {
      if (nums[i] != val) {
        nums[left++] = nums[i];
      }
    }

    return left;
  }

  public static void main(String[] args) {
    int[] nums = new int[] {3, 3};
    int i = new Solution2().removeElement(nums, 3);
    System.out.println(Arrays.toString(nums));
    System.out.println(i);
  }
}
/**
 * 使用指针，只考虑如何存储不同元素；
 * 指针取到的值不想等，就覆盖。 **快慢指针**
 */
