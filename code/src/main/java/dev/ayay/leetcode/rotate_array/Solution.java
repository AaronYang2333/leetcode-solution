package dev.ayay.leetcode.rotate_array;

import java.util.Arrays;

public class Solution {
  public void rotate(int[] nums, int k) {
    k = k % nums.length;
    int[] arr = new int[nums.length + k];
    System.arraycopy(nums, 0, arr, k, nums.length);
    for (int i = 0; i < k; i++) {
      arr[i] = arr[i + nums.length];
    }
    System.arraycopy(arr, 0, nums, 0, nums.length);
  }

  public static void main(String[] args) {
    int[] ints = {1, 2, 3, 4, 5, 6, 7};
    new Solution().rotate(ints, 3);
    System.out.println(Arrays.toString(ints));
  }
}
