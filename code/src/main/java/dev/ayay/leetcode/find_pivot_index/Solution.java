package dev.ayay.leetcode.find_pivot_index;

import java.util.Arrays;

public class Solution {
  public int pivotIndex(int[] nums) {

    for (int i = 0; i < nums.length; i++) {
      int left = i == 0 ? 0 : Arrays.stream(Arrays.copyOfRange(nums, 0, i)).sum();
      int right =
          i == nums.length - 1
              ? 0
              : Arrays.stream(Arrays.copyOfRange(nums, i + 1, nums.length)).sum();
      if (left == right) {
        return i;
      }
    }

    return -1;
  }

  public static void main(String[] args) {
            System.out.println(new Solution().pivotIndex(new int[]{2, 1, -1}));
//    System.out.println(new Solution().pivotIndex(new int[] {1, 7, 3, 6, 5, 6}));
  }
}
