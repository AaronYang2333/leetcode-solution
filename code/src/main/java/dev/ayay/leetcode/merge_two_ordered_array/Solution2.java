package dev.ayay.leetcode.merge_two_ordered_array;

import java.util.Arrays;

/**
 * https://leetcode.cn/problems/merge-sorted-array/?envType=study-plan-v2&envId=top-interview-150
 */
public class Solution2 {
  /**
   * 通过在数组内移动方式 避免额外空间占用
   * 空间是省下来了，但是时间慢了，因为涉及到位置移动
   * @param nums1
   * @param m
   * @param nums2
   * @param n
   */
  public void merge(int[] nums1, int m, int[] nums2, int n) {
    for (int i = 0; i < n; i++) {
      boolean inserted = false;
      for (int j = 0; j < m + i; j++) {
        if (nums1[j] >= nums2[i]) {
          System.arraycopy(nums1, j, nums1, j + 1, nums1.length - j - 1);
          nums1[j] = nums2[i];
          inserted = true;
          break;
        }
      }
      if (!inserted) {
        nums1[i + m] = nums2[i];
      }
    }
  }

  public static void main(String[] args) {
    int[] nums1 = new int[] {1, 2, 3, 0, 0, 0};
    int[] nums2 = new int[] {2, 5, 6};
    new Solution2().merge(nums1, 3, nums2, 3);
    System.out.printf(Arrays.toString(nums1)); // [1, 2, 2, 3, 5, 6]
  }
}
