package dev.ayay.leetcode.merge_two_ordered_array;

import java.util.Arrays;

/**
 * https://leetcode.cn/problems/merge-sorted-array/?envType=study-plan-v2&envId=top-interview-150
 * 给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。
 *
 * 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。
 */
public class Solution {
  /**
   * 通过创建额外数组完成两个数组的合并
   * TODO 是不是可以在数组内移动方式 避免额外空间占用
   *
   * @param nums1
   * @param m
   * @param nums2
   * @param n
   */
  public void merge(int[] nums1, int m, int[] nums2, int n) {
    int i = 0;
    int j = 0;
    int k = 0;
    int[] temp = Arrays.copyOf(nums1, m);

    while (i < m && j < n) {
      if (temp[i] <= nums2[j]) {
        nums1[k] = temp[i];
        i++;
        k++;
      } else {
        nums1[k] = nums2[j];
        j++;
        k++;
      }
    }
    if (i == m) {
      for (; j < n; j++) {
        nums1[k] = nums2[j];
        k++;
      }
    }
    if (j == n) {
      for (; i < m; i++) {
        nums1[k] = temp[i];
        k++;
      }
    }
  }

  public static void main(String[] args) {
    int[] nums1 = new int[] {1, 2, 3, 0, 0, 0};
    int[] nums2 = new int[] {2, 5, 6};
    new Solution().merge(nums1, 3, nums2, 3);
    System.out.printf(Arrays.toString(nums1)); // [1, 2, 2, 3, 5, 6]
  }
}
