package dev.ayay.leetcode.majority_element;

import java.util.HashSet;

public class Solution2 {
  public int majorityElement(int[] nums) {
    HashSet<Integer> hashSet = new HashSet<Integer>();
    for (int num : nums) {
      if (hashSet.contains(num)) {
        hashSet.remove(num);
      } else {
        hashSet.add(num);
      }
    }
    return hashSet.iterator().next();
  }
}
