package dev.ayay.leetcode.majority_element;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Solution {
  public int majorityElement(int[] nums) {
    Map<Integer, Integer> map = new HashMap<Integer, Integer>();
    for (int num : nums) {
      if (map.containsKey(num)) {
        map.put(num, map.get(num) + 1);
      } else {
        map.put(num, 1);
      }
    }

    AtomicInteger result = new AtomicInteger();
    map.forEach(
        (k, v) -> {
          if (v > nums.length / 2) {
            result.set(k);
          }
        });
    return result.get();
  }
}
